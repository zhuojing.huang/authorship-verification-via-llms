# README

This repository contains two Python notebooks (`T5Ngram.ipynb` and `human_test_analysis`).
-`T5Ngram.ipynb` demonstrates the process of generating and analyzing ngrams from a given dataset of sentences. 
-`human_test_analysis`analyse the json file returned from the human test conducted through AVER. 
Note: The notebook `T5Ngram.ipynb` was set to analyse trigrams, but simply by changing some parameters, it can be used to analyse bigrams. Below is an overview of the functionalities implemented in the notebook:

## Table of Contents

- [Introduction](#introduction)
- [Dependencies](#dependencies)
- [Data](#data)
- [Usage](#usage)
- [Functionality Overview](#functionality-overview)
- [Results](#results)

## Introduction<a name="introduction"></a>

The notebook shows the steps I took for the project. It demonstrates how to extract ngrams from sentences, replace certain ngrams with placeholders, use a language model to predict words for the placeholders, evaluate the predictions, and analyze the part-of-speech (POS) patterns of the predicted ngrams.

## Dependencies<a name="dependencies"></a>

The notebook utilizes several Python libraries and tools, including:

- NLTK (Natural Language Toolkit) for text processing tasks such as tokenization, part-of-speech tagging, and named entity recognition.
- Pandas for data manipulation and analysis.
- Matplotlib for data visualization.
- Transformers library for utilizing pre-trained language models, particularly the T5 model.
- Sentencepiece for tokenization with T5 model.

## Data<a name="data"></a>
The repo also include datasets I used for the project, which are placed in the `data` dir.
- `MSR dataset`: each entry has a short paragraph, suitable for creating cloze test for LLMs
- `ngrams_words_2`: top 100000 frequent bigrams
- `ngrams_words_3`: top 100000 frequent trigrams
- `ngram-experiment_export`: json file returned from the human test on AVER

## Usage<a name="usage"></a>

To run the notebook, you need to have the following components:

1. Python environment with the necessary libraries installed. You can install the dependencies using `pip`.
2. Access to the provided dataset or replace it with your own dataset. The datasets I used is included in the repo.

## Functionality Overview<a name="functionality-overview"></a>

The notebook performs the following main tasks:

1. **Data Preprocessing**: It reads the dataset, processes the text by removing punctuation and special characters, and extracts ngrams from the sentences.

2. **Ngram Replacement**: Certain ngrams in the sentences are replaced with placeholders (`<<BLANK>> <<BLANK>> <<BLANK>>`)(or `<<BLANK>> <<BLANK>>` if it is bigram that is to be replaced) to simulate missing information.

3. **Language Model Prediction**: The T5 language model is used to predict words for the placeholders. The notebook provides functions to perform this task.

4. **Evaluation**: The predicted sentences are compared with the original sentences to evaluate the accuracy of the predictions.

5. **Analysis**: Finally, the notebook analyzes the POS patterns of the predicted ngrams and visualizes the most frequent POS ngrams.

## Results<a name="results"></a>

The notebook generates visualizations to illustrate the distribution of correctly guessed ngrams and the frequency of POS ngrams. These results provide insights into the language model's performance and the POS patterns in cloze test.

For more detailed information, please refer to the notebook itself.